# sshmount

include config.mk

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f wafire $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/wafire

build:
	@read -p "Enter WebApp Name: " name; \
	read -p "Enter WebApp url: " url; \
	read -p "Enter icon filetype (default png): " img; \
	cp DesktopFile $$name.desktop; \
	sed -i "s/WaFire/$$name/" $$name.desktop; \
	sed -i "s/Url/$$url/" $$name.desktop; \
	cp -f $$name.desktop /usr/share/applications/; \
	[ -f $$name.$${img:-png} ] || echo "warning: no icon by the name of $$name.png available"; \
	[ -f $$name.$${img:-png} ] && cp -f $$name.$${img:-png} /usr/share/icons/; \
	chmod 644 /usr/share/applications/$$name.desktop

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/wafire

.PHONY: install uninstall

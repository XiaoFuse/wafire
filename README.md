# Wafire launch firefox in kiosk mode
wafire is a really really simple script that launches firefox in kiosk mode, that is no addressbar no menu's and such just you and the website
it's a one liner but it makes it convinient to just write `wafire soundcloud.com` to launch it in this mode

to set it up just run `sudo make install` to install the script into `/usr/local/bin`
the included soundcloud.desktop can be placed in `/usr/share/applications/` simply running `sudo make soundcloud`
other potential websites could be wrapped in a desktop file by adding a make target in Makefile similar to what is already there

## todo
fix icon and classname
